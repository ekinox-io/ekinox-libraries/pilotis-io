from pathlib import Path

import pandas as pd
import pytest
from hamcrest import assert_that, greater_than, instance_of
from hamcrest.core.core import is_
from sklearn.tree import DecisionTreeClassifier

from pilotis_io.directory_structure import dataset_parsed_dir_path, dataset_raw_dir_path
from pilotis_io.io import IoAPI
from pilotis_io.local import LocalIoApi, LocalPandasApi
from pilotis_io.pandas import PandasApi
from pilotis_io.persistence import (
    LandingDataSourcePersistence,
    UseCaseDataSetPersistence,
    UseCaseExportPersistence,
    UseCaseModelPersistence,
)


@pytest.fixture
def local_api(tmp_path: Path) -> IoAPI:
    return LocalIoApi(project_dir=str(tmp_path))


@pytest.fixture
def pandas_api(tmp_path: Path) -> PandasApi:
    io_api = LocalIoApi(project_dir=str(tmp_path))
    return LocalPandasApi(io_api)


class TestLandingDataSource(LandingDataSourcePersistence):
    def load_raw(self, dataset_version: str = None) -> pd.DataFrame:
        dataset_files = self.io_api.list_files_in_dir(
            dataset_raw_dir_path(self.dataset_name, dataset_version)
        )
        return self.pandas_api.load_pandas_dataset(dataset_files)


def test_save_reload_parsed_should_deal_with_parsed_files(
    local_api: IoAPI, pandas_api: PandasApi
):
    # Given a dataset
    dataset_name = "dataset_name"
    dataset_version = "dataset_version"
    sample_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # And a persistence definition for this dataset
    persistence = TestLandingDataSource(local_api, pandas_api, dataset_name)

    # When saving parsed
    persistence.save_parsed(sample_df, dataset_version)

    # Then It creates some files
    parsed_dir = local_api.project_root_path / dataset_parsed_dir_path(
        dataset_name, dataset_version
    )
    assert_that(parsed_dir.exists(), is_(True))
    assert_that(parsed_dir.is_dir(), is_(True))
    assert_that(len(list(parsed_dir.glob("**/*"))), greater_than(0))

    # When reloading it
    loaded_df = persistence.load_parsed(dataset_version)

    # Then it is the same as the original
    pd.testing.assert_frame_equal(sample_df, loaded_df)


def test_save_and_reload_use_case_datasource_should_deal_with_its_files(
    local_api: IoAPI, pandas_api: PandasApi
):  # noqa
    # Given a dataset
    dataset_name = "dataset_name"
    dataset_version = "dataset_version"
    sample_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # And a persistence definition for this dataset
    use_case_name = "use_case"
    persistence = UseCaseDataSetPersistence(
        local_api, pandas_api, use_case_name, dataset_name
    )

    # When saving parsed
    persistence.save_dataset(sample_df, dataset_version)

    # Then It creates some files
    dataset_relative_path = persistence.dataset_path(dataset_version)
    dataset_path = local_api.project_root_path / dataset_relative_path
    assert_that(dataset_path.exists(), is_(True))

    # When reloading it
    loaded_df = persistence.load_dataset(dataset_version)

    # Then it is the same as the original
    pd.testing.assert_frame_equal(sample_df, loaded_df)


def test_save_and_reload_models_should_deal_with_its_files(local_api: IoAPI):
    # Given an sklearn model
    model = DecisionTreeClassifier()
    model_name = "test_model"
    model_run_id = "some_run_id"

    # And a persistence definition for this model
    use_case_name = "use_case"
    model_persistence = UseCaseModelPersistence(local_api, use_case_name, model_name)

    # When saving model
    model_persistence.save_sklearn_model(model, model_run_id)

    # Then a file is created
    model_relative_path = model_persistence.model_path(model_run_id)
    model_absolute_path = local_api.project_root_path / model_relative_path
    assert_that(model_absolute_path.exists(), is_(True))

    # When reloading model
    restored_model = model_persistence.load_sklearn_model(model_run_id)

    # Then it is a model that is loaded
    assert_that(restored_model, instance_of(DecisionTreeClassifier))


def test_save_and_reload_use_case_export_should_deal_with_its_files(
    local_api: IoAPI, pandas_api: PandasApi
):  # noqa
    # Given a dataset
    dataset_name = "dataset_name"
    dataset_version = "dataset_version"
    sample_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # And a persistence definition for this dataset
    use_case_name = "use_case"
    persistence = UseCaseExportPersistence(
        io_api=local_api,
        pandas_api=pandas_api,
        use_case_name=use_case_name,
        export_name=dataset_name,
        export_file_name="my_export.csv",
    )

    # When saving parsed
    persistence.export_pandas_dataframe(export_df=sample_df, export_id=dataset_version)

    # Then It creates some files
    dataset_relative_path = persistence.export_path(export_id=dataset_version)
    dataset_path = local_api.project_root_path / dataset_relative_path
    assert_that(dataset_path.exists(), is_(True))

    # When reloading it
    loaded_df = persistence.reload_pandas_export(export_id=dataset_version)

    # Then it is the same as the original
    pd.testing.assert_frame_equal(sample_df, loaded_df)
