from pathlib import Path
from typing import List, Optional

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import pytest
from hamcrest.core import assert_that
from pandas.util.testing import assert_frame_equal

from pilotis_io.exceptions import PilotisIoError
from pilotis_io.local import LocalIoApi, LocalPandasApi
from pilotis_io.pandas import PandasApi


@pytest.fixture
def pandas_api(tmp_path: Path) -> PandasApi:
    io_api = LocalIoApi(project_dir=str(tmp_path))
    return LocalPandasApi(io_api)


def test_save_pandas_dataset_csv(pandas_api: PandasApi):
    # Given a pandas dataset
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to CSV
    export_path: Path = Path("export") / Path("file.csv")
    pandas_api.save_pandas_dataset(df, export_path)

    # Then a CSV file must exists
    full_path = pandas_api.io_api.project_root_path / export_path
    assert_that(full_path.exists())
    assert_that(full_path.is_file())
    csv_dataframe = pd.read_csv(str(full_path))
    assert_frame_equal(df, csv_dataframe)


def test_save_pandas_dataset_parquet(pandas_api: PandasApi):
    # Given a pandas dataset
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to CSV
    export_path: Path = Path("export") / Path("file.parquet")
    pandas_api.save_pandas_dataset(df, export_path)

    # Then a CSV file must exists
    full_path = pandas_api.io_api.project_root_path / export_path
    assert_that(full_path.exists())
    assert_that(full_path.is_file())
    parquet_table = pq.read_table(str(full_path))
    assert_frame_equal(df, parquet_table.to_pandas())


def test_save_pandas_dataset_unknown_format(pandas_api: PandasApi):
    # Given a pandas dataset
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to a non supported format
    # Then it should raise an exception
    export_path: Path = Path("export") / Path("file.unknown")
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)

    # And no file is generated
    full_path = pandas_api.io_api.project_root_path / export_path
    assert_that(not full_path.exists())


def test_save_pandas_dataset_with_no_dataset(pandas_api: PandasApi):
    # Given None as the dataset
    df: Optional[pd.DataFrame] = None

    # When saving it to CSV
    # Then it should raise an exception
    export_path: Path = Path("export") / Path("file.csv")
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)

    # And no file is generated
    full_path = pandas_api.io_api.project_root_path / export_path
    assert_that(not full_path.exists())


def test_save_pandas_dataset_with_no_destination_path(pandas_api: PandasApi):
    # Given None as the dataset
    df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})

    # When saving it to a non set destination
    # Then it should raise an exception
    export_path: Optional[Path] = None
    with pytest.raises(PilotisIoError):
        pandas_api.save_pandas_dataset(df, export_path)


def test_load_pandas_dataset_with_one_csv_file(pandas_api: PandasApi):
    # Given a CSV file inside the API's directory
    csv_relative_path = Path("export.csv")
    csv_full_path = pandas_api.io_api.project_root_path / csv_relative_path
    csv_full_path.write_text(data="col1,col2\n1,3\n2,4\n")

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(relative_file_paths=[csv_relative_path])

    # Then the data is in memory
    expected_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    assert_frame_equal(expected_df, df)


def test_load_pandas_dataset_with_two_csv_file(pandas_api: PandasApi):
    # Given two CSV files inside the API's directory
    csv_1_relative_path = Path("export_1.csv")
    csv_1_full_path = pandas_api.io_api.project_root_path / csv_1_relative_path
    csv_1_full_path.write_text(data="col1,col2\n1,3\n")
    csv_2_relative_path = Path("export_2.csv")
    csv_2_full_path = pandas_api.io_api.project_root_path / csv_2_relative_path
    csv_2_full_path.write_text(data="col1,col2\n2,4\n")

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(
        relative_file_paths=[csv_1_relative_path, csv_2_relative_path]
    )

    # Then the data is in memory
    expected_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    assert_frame_equal(expected_df.reset_index(drop=True), df.reset_index(drop=True))


def test_load_pandas_dataset_with_one_parquet_file(pandas_api: PandasApi):
    # Given a Parquet file inside the API's directory
    parquet_relative_path = Path("export.parquet")
    export_df = pd.DataFrame(data={"col1": [1, 2], "col2": [3, 4]})
    table = pa.Table.from_pydict(mapping={"col1": [1, 2], "col2": [3, 4]})  # noqa
    parquet_full_path = pandas_api.io_api.project_root_path / parquet_relative_path
    pq.write_table(table=table, where=str(parquet_full_path))

    # When loading it as a pandas DF
    df = pandas_api.load_pandas_dataset(relative_file_paths=[parquet_relative_path])

    # Then the data is in memory
    assert_frame_equal(export_df, df)


def test_load_pandas_dataset_with_one_unknown_format_file(pandas_api: PandasApi):
    # Given a CSV file with an unknown format
    csv_relative_path = Path("export.my_custom_csv")
    csv_full_path = pandas_api.io_api.project_root_path / csv_relative_path
    csv_full_path.write_text(data="col1,col2\n1,3\n2,4\n")

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=[csv_relative_path])


def test_load_pandas_dataset_with_no_file_to_load(pandas_api: PandasApi):
    # Given an empty list of files to load
    relative_paths: List[Path] = []

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=relative_paths)


def test_load_pandas_dataset_with_none_as_the_files_to_load(
    pandas_api: PandasApi,
):
    # Given an null list of files to load
    relative_paths: Optional[List[Path]] = None

    # When loading it as a pandas DF
    # Then it should raise an error
    with pytest.raises(PilotisIoError):
        pandas_api.load_pandas_dataset(relative_file_paths=relative_paths)
