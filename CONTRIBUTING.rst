.. highlight:: shell

============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at https://gitlab.com/ekinox-io/ekinox-libraries/ekinox-python-library-for-jacc/-/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the GitLab issues for bugs. Anything tagged with "bug" and "help
wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the GitLab issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

Pilotis IO could always use more documentation, whether as part of the
official Pilotis IO docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at
https://gitlab.com/ekinox-io/ekinox-libraries/ekinox-python-library-for-jacc/-/issues.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

Get Started!
------------

Ready to contribute? Here's how to set up ``pilotis-io`` for local development on a Mac.

Prerequisite
~~~~~~~~~~~~

* python 3.6, 3.7 and 3.8 with `pyenv <https://github.com/pyenv/pyenv>`_

* `poetry <https://poetry.eustace.io/>`_

* `pre-commit <https://pre-commit.com/>`_ (optional but useful)

Setup (for Mac)
~~~~~~~~~~~~~~~

1. Fork the ``pilotis-io`` repo on GitLab.
2. Clone your fork locally::

    $ git clone https://gitlab.com/your-namespace/ekinox-python-library-for-jacc.git

3. Assuming you have the prerequisites installed, this is how you set up your fork for local development::

    $ cd ekinox-python-library-for-jacc/
    $ make setup-dev-env-full

4. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

5. When you're done making changes, check that your changes pass typing, linting, formatting, unit tests
   (for all versions of Python) and functional (bdd) tests::

    $ make tox

.. note::

    See the Makefile help for all available targets.

6. Commit your changes and push your branch to GitLab::

    $ git add .
    $ git commit -m "[feat|fix|chore|...]: your detailed description of your changes"
    $ git push --set-upstream origin name-of-your-bugfix-or-feature

7. Submit a merge request through the GitLab website.

Merge Request Guidelines
------------------------

Before you submit a merge request, check that it meets these guidelines:

1. The merge request should include tests.
2. If the merge request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.rst.
3. The merge request should pass on all tests and checks of the pipeline.
